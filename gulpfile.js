const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');

const paths = {
  styles: {
    src: 'src/scss/styles.scss',
    dest: 'public_html/css'
  }
};

function styles() {
  return gulp.src(paths.styles.src, { allowEmpty: true })
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(cleanCSS())
    .pipe(rename({
      basename: 'main',
      suffix: '.min'
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.styles.dest));
}

function watch() {
  gulp.watch('src/scss/**/*.scss', styles);
}

const build = gulp.series(styles, watch);

exports.styles = styles;
exports.watch = watch;
exports.build = build;
exports.default = build;
