$wt.charts
	.render({
		data: {
			chart: {
				type: "area",
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy"
        },
        panKey: "shift",
			},
			colors: ["#BCBC01", "#39B0B5", "#4C7DBF", "#e6aa82", "#829bcd", "#fae6c3", "#aab9e1"],
			legend: {
				borderRadius: 4,
			},
			plotOptions: {
				series: {
					stacking: "normal",
					dataLabels: {
						enabled: true,
						style: {
							fontWeight: "bold",
						},
					},
				},
			},
			series: [
				{
					name: "Purchase of vehicles",
					data: [
						{
							name: "2020",
							y: 3.8,
						},
						{
							name: "2019",
							y: 3.8,
						},
						{
							name: "2018",
							y: 3.8,
						},
						{
							name: "2017",
							y: 3.7,
						},
						{
							name: "2016",
							y: 3.6,
						},
						{
							name: "2015",
							y: 3.4,
						},
						{
							name: "2014",
							y: 3.2,
						},
						{
							name: "2013",
							y: 3.1,
						},
						{
							name: "2012",
							y: 3.2,
						},
						{
							name: "2011",
							y: 3.4,
						},
						{
							name: "2010",
							y: 3.5,
						},
					],
				},
				{
					name: "Operation of personal transport equipment",
					data: [
						{
							name: "2020",
							y: 6.4,
						},
						{
							name: "2019",
							y: 7.1,
						},
						{
							name: "2018",
							y: 7.1,
						},
						{
							name: "2017",
							y: 6.9,
						},
						{
							name: "2016",
							y: 6.7,
						},
						{
							name: "2015",
							y: 6.9,
						},
						{
							name: "2014",
							y: 7.3,
						},
						{
							name: "2013",
							y: 7.4,
						},
						{
							name: "2012",
							y: 7.6,
						},
						{
							name: "2011",
							y: 7.5,
						},
						{
							name: "2010",
							y: 7.2,
						},
					],
				},
				{
					name: "Transport services",
					data: [
						{
							name: "2020",
							y: 1.4,
						},
						{
							name: "2019",
							y: 2.3,
						},
						{
							name: "2018",
							y: 2.3,
						},
						{
							name: "2017",
							y: 2.3,
						},
						{
							name: "2016",
							y: 2.2,
						},
						{
							name: "2015",
							y: 2.2,
						},
						{
							name: "2014",
							y: 2.2,
						},
						{
							name: "2013",
							y: 2.2,
						},
						{
							name: "2012",
							y: 2.2,
						},
						{
							name: "2011",
							y: 2.1,
						},
						{
							name: "2010",
							y: 2,
						},
					],
				},
			],
			title: {
				text: "Development of share of household expenditure on transport",
				style: {
					fontFamily: "Arial",
					fontSize: "14px",
				},
			},
			subtitle: {
				text: "(%, EU, 2010–2020)",
				style: {
					fontFamily: "Arial",
				},
			},
			xAxis: {
				type: "category",
				gridLineColor: "rgba(255,255,255,0.3)",
			},
			yAxis: {
				gridLineColor: "rgba(255,255,255,0.3)",
			},
		},
	})
	.ready(function (chart) {
	})
