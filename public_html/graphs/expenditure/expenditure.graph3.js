$wt.charts
	.render({
		data: {
			chart: {
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy"
        },
        panKey: "shift",
			},
			colors: ["#4C7DBF", "#39B0B5", "#BCBC01", "#e6aa82", "#829bcd", "#fae6c3", "#aab9e1"],
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [
				{
					name: "European Union - 27 countries (from 2020)",
					data: [
						{
							name: "2010",
							y: 2.3,
						},
						{
							name: "2011",
							y: 2.2,
						},
						{
							name: "2012",
							y: 2.1,
						},
						{
							name: "2013",
							y: 2.1,
						},
						{
							name: "2014",
							y: 2.1,
						},
						{
							name: "2015",
							y: 2.1,
						},
						{
							name: "2016",
							y: 2,
						},
						{
							name: "2017",
							y: 2,
						},
						{
							name: "2018",
							y: 2,
						},
						{
							name: "2019",
							y: 2,
						},
						{
							name: "2020",
							y: 2.3,
						},
					],
				},
			],
			xAxis: {
				type: "category",
				gridLineColor: "#CFDAF5",
			},
			yAxis: {
				title: {
					text: "% of GDP",
				},
				gridLineColor: "#CFDAF5",
			},
		},
	})
	.ready(function (chart) {})
