$wt.charts
	.render({
		data: {
			chart: {
				type: "scatter",
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy"
        },
        panKey: "shift",
			},
      colors: ["#BCBC01", "#39B0B5", "#4C7DBF", "#e6aa82", "#829bcd", "#fae6c3", "#aab9e1"],
			plotOptions: {
				series: {
					marker: {
						symbol: "circle",
					},
					dataLabels: {
						style: {
							fontWeight: "bold",
						},
					},
				},
			},
			series: [
				{
					name: "Government - 2020",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: 2.3,
							id: "eu",
						},
						{
							name: "Belgium",
							y: 3,
						},
						{
							name: "Bulgaria",
							y: 3.7,
						},
						{
							name: "Czechia",
							y: 4.1,
						},
						{
							name: "Denmark",
							y: 2.1,
						},
						{
							name: "Germany",
							y: 1.7,
						},
						{
							name: "Estonia",
							y: 3,
						},
						{
							name: "Ireland",
							y: 1.2,
						},
						{
							name: "Greece",
							y: 2.1,
						},
						{
							name: "Spain",
							y: 1.9,
						},
						{
							name: "France",
							y: 2.2,
						},
						{
							name: "Croatia",
							y: 3.9,
						},
						{
							name: "Italy",
							y: 2,
						},
						{
							name: "Cyprus",
							y: 0.7,
						},
						{
							name: "Latvia",
							y: 4.4,
						},
						{
							name: "Lithuania",
							y: 2.1,
						},
						{
							name: "Luxembourg",
							y: 3.4,
						},
						{
							name: "Hungary",
							y: 5.2,
						},
						{
							name: "Malta",
							y: 2.7,
						},
						{
							name: "Netherlands",
							y: 2.4,
						},
						{
							name: "Austria",
							y: 3.1,
						},
						{
							name: "Poland",
							y: 3.7,
						},
						{
							name: "Portugal",
							y: 2.6,
						},
						{
							name: "Romania",
							y: 3.3,
						},
						{
							name: "Slovenia",
							y: 2.7,
						},
						{
							name: "Slovakia",
							y: 3.8,
						},
						{
							name: "Finland",
							y: 2.5,
						},
						{
							name: "Sweden",
							y: 3,
						},
						{
							name: "Iceland",
							y: 2.9,
						},
						{
							name: "Norway",
							y: 4.4,
						},
						{
							name: "Switzerland",
							y: 2.7,
						},
					],
				},
				{
					name: "Government - 2019",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: 2,
						},
						{
							name: "Belgium",
							y: 2.8,
						},
						{
							name: "Bulgaria",
							y: 3.4,
						},
						{
							name: "Czechia",
							y: 3.6,
						},
						{
							name: "Denmark",
							y: 1.8,
						},
						{
							name: "Germany",
							y: 1.6,
						},
						{
							name: "Estonia",
							y: 2.6,
						},
						{
							name: "Ireland",
							y: 1.1,
						},
						{
							name: "Greece",
							y: 1.9,
						},
						{
							name: "Spain",
							y: 1.5,
						},
						{
							name: "France",
							y: 2,
						},
						{
							name: "Croatia",
							y: 3.3,
						},
						{
							name: "Italy",
							y: 1.8,
						},
						{
							name: "Cyprus",
							y: 0.6,
						},
						{
							name: "Latvia",
							y: 3.1,
						},
						{
							name: "Lithuania",
							y: 1.6,
						},
						{
							name: "Luxembourg",
							y: 3.2,
						},
						{
							name: "Hungary",
							y: 5.2,
						},
						{
							name: "Malta",
							y: 1.9,
						},
						{
							name: "Netherlands",
							y: 2.1,
						},
						{
							name: "Austria",
							y: 2.8,
						},
						{
							name: "Poland",
							y: 3.4,
						},
						{
							name: "Portugal",
							y: 1.8,
						},
						{
							name: "Romania",
							y: 3,
						},
						{
							name: "Slovenia",
							y: 2.5,
						},
						{
							name: "Slovakia",
							y: 3.8,
						},
						{
							name: "Finland",
							y: 2.2,
						},
						{
							name: "Sweden",
							y: 2.8,
						},
						{
							name: "Iceland",
							y: 2.8,
						},
						{
							name: "Norway",
							y: 3.9,
						},
						{
							name: "Switzerland",
							y: 2.5,
						},
					],
				},
				{
					name: "Household - 2019",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: 13.2,
						},
						{
							name: "Belgium",
							y: 11.2,
						},
						{
							name: "Bulgaria",
							y: 13.8,
						},
						{
							name: "Czechia",
							y: 10.2,
						},
						{
							name: "Denmark",
							y: 12.4,
						},
						{
							name: "Germany",
							y: 14.1,
						},
						{
							name: "Estonia",
							y: 11.8,
						},
						{
							name: "Ireland",
							y: 12.3,
						},
						{
							name: "Greece",
							y: 12.3,
						},
						{
							name: "Spain",
							y: 12.3,
						},
						{
							name: "France",
							y: 14,
						},
						{
							name: "Croatia",
							y: 9.3,
						},
						{
							name: "Italy",
							y: 13,
						},
						{
							name: "Cyprus",
							y: 15,
						},
						{
							name: "Latvia",
							y: 11.7,
						},
						{
							name: "Lithuania",
							y: 15.7,
						},
						{
							name: "Luxembourg",
							y: 14.8,
						},
						{
							name: "Hungary",
							y: 12.6,
						},
						{
							name: "Malta",
							y: 11.3,
						},
						{
							name: "Netherlands",
							y: 12.3,
						},
						{
							name: "Austria",
							y: 12.1,
						},
						{
							name: "Poland",
							y: 14.5,
						},
						{
							name: "Portugal",
							y: 13.6,
						},
						{
							name: "Romania",
							y: 12.2,
						},
						{
							name: "Slovenia",
							y: 17.1,
						},
						{
							name: "Slovakia",
							y: 6.5,
						},
						{
							name: "Finland",
							y: 11.7,
						},
						{
							name: "Sweden",
							y: 12.9,
						},
						{
							name: "Iceland",
							y: 13,
						},
						{
							name: "Norway",
							y: 15.6,
						},
						{
							name: "Switzerland",
							y: 9.3,
						},
					],
				},
				{
					name: "Household - 2020",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: 11.6,
						},
						{
							name: "Belgium",
							y: 10.1,
						},
						{
							name: "Bulgaria",
							y: 11.7,
						},
						{
							name: "Czechia",
							y: 9.1,
						},
						{
							name: "Denmark",
							y: 11.3,
						},
						{
							name: "Germany",
							y: 13,
						},
						{
							name: "Estonia",
							y: 9.9,
						},
						{
							name: "Ireland",
							y: 10.5,
						},
						{
							name: "Greece",
							y: 10,
						},
						{
							name: "Spain",
							y: 10.4,
						},
						{
							name: "France",
							y: 11.7,
						},
						{
							name: "Croatia",
							y: 7.4,
						},
						{
							name: "Italy",
							y: 11.1,
						},
						{
							name: "Cyprus",
							y: 12,
						},
						{
							name: "Latvia",
							y: 10.2,
						},
						{
							name: "Lithuania",
							y: 14.1,
						},
						{
							name: "Luxembourg",
							y: 13.2,
						},
						{
							name: "Hungary",
							y: 11.2,
						},
						{
							name: "Malta",
							y: 11,
						},
						{
							name: "Netherlands",
							y: 11.2,
						},
						{
							name: "Austria",
							y: 10.6,
						},
						{
							name: "Poland",
							y: 13.1,
						},
						{
							name: "Portugal",
							y: 11.3,
						},
						{
							name: "Romania",
							y: 11.1,
						},
						{
							name: "Slovenia",
							y: 14.2,
						},
						{
							name: "Slovakia",
							y: 4.9,
						},
						{
							name: "Finland",
							y: 10.4,
						},
						{
							name: "Sweden",
							y: 11.3,
						},
						{
							name: "Iceland",
							y: 12,
						},
						{
							name: "Norway",
							y: 13.5,
						},
						{
							name: "Switzerland",
							y: 8.6,
						},
					],
				},
        {
          name: 'Highest change',
          color: '#8e9ec4',
          marker: {
            symbol: 'square'
          },
          data: [],
          showInLegend: true
        }
			],
			title: {
				text: "Share of household  and government expenditure on transport",
				style: {
					fontFamily: "Arial",
					fontSize: "14px",
				},
			},
			subtitle: {
				text: "(%, 2019 and 2020)",
				style: {
					fontFamily: "Arial",
				},
			},
			tooltip: {
				pointFormat: '<span style="color:{point.color}">●</span> {point.name}: <b>{point.y}</b><br/>',
			},
			xAxis: {
				type: "category",
				gridLineWidth: 1,
				gridLineColor: "#CFDAF5",
				plotBands: [
					{
						color: "#8e9ec4",
						from: 23.5, // Start of the plot band
						to: 24.5, // End of the plot band
						id: "plot-band-1",
						label: {
							text: "Highest change",
							style: {
                opacity: 0
							},
						},
					},
				],
			},
			yAxis: {
				gridLineColor: "#CFDAF5",
			},
		},
	})
	.ready(function (chart) {
		const plotBandId = "plot-band-1"
		const plotBand = chart.xAxis[0].plotLinesAndBands.find(function (pl) {
			return pl.id === plotBandId
		})

		let plotBandTooltip

		// add mouseover event to the plot band
		plotBand.svgElem.on("mouseover", showTooltip)

		// add mouseout event to the plot band
		plotBand.svgElem.on("mouseout", hideTooltip)

		// add mouseover event to the x-axis labels
		Array.from(chart.container.querySelectorAll(".highcharts-xaxis-labels text")).forEach(function (label) {
			label.addEventListener("mouseover", function () {
				if (this.textContent == chart.xAxis[0].categories[Math.round(plotBand.options.from)]) {
					showTooltip.call(plotBand.svgElem.element)
				}
			})

			// add mouseout event to the x-axis labels
			label.addEventListener("mouseout", hideTooltip)
		})

		function showTooltip() {
			if (!plotBandTooltip) {
				const text = plotBand.options.label.text
				const x = chart.xAxis[0].toPixels(plotBand.options.from)
				const y = chart.plotTop

				plotBandTooltip = chart.renderer.label(text, x, y, "callout", x, y).attr({ align: "center", padding: 10, r: 5, fill: "#f7f7f7", zIndex: 8, fontSize: "10px" }).css({ color: "#333333" }).add()
			}
		}

		function hideTooltip() {
			if (plotBandTooltip) {
				plotBandTooltip.destroy()
				plotBandTooltip = null
			}
		}
	})
