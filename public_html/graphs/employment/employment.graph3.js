// Create a chart using the native API from Highcharts.
const chart = Highcharts.chart("employment-ch3", {
	chart: {
		type: "pie", // zooming is not supported in pie chart
    backgroundColor: "#F3F6FC",
    marginTop: 80,
    marginBottom: 20
	},
	title: {
		text: "Distribution of employment in the transport subsector by sex 2021",
	},
	tooltip: {
		pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
	},
	credits: {
		enabled: false,
	},
	accessibility: {
		point: {
			valueSuffix: "%",
		},
	},
	plotOptions: {
		pie: {
			dataLabels: {
				enabled: true,
				format: "<b>{point.name}</b>: {point.percentage:.1f} %",
			},
		},
	},
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
  },
	series: [
		{
			data: [],
		},
	]
})

const container = document.querySelector("#employment-ch3")
const commands = document.querySelector("#commands")
container.style.display = "block"
commands.style.display = "block"

const landBtn = document.querySelector("#land")
landBtn.addEventListener("click", function (event) {
	chart.update({
		subtitle: {
			text: "Land",
		},
		series: [
			{
        name: "Land",
				data: [
					{
						name: "Male",
						y: 84.8,
						color: "#0E47CB",
					},
					{
						name: "Female",
						y: 15.2,
						color: "#E746E1",
					},
				],
			},
		],
	})
})

const waterBtn = document.querySelector("#water")
waterBtn.addEventListener("click", function (event) {
	chart.update({
		subtitle: {
			text: "Water",
		},
		series: [
			{
        name: "Water",
				data: [
					{
						name: "Male",
						y: 78.1,
						color: "#0E47CB",
					},
					{
						name: "Female",
						y: 21.9,
						color: "#E746E1",
					},
				],
			},
		],
	})
})

const airBtn = document.querySelector("#air")
airBtn.addEventListener("click", function (event) {
	chart.update({
		subtitle: {
			text: "Air",
		},
		series: [
			{
        name: "Air",
				data: [
					{
						name: "Male",
						y: 58,
						color: "#0E47CB",
					},
					{
						name: "Female",
						y: 42,
						color: "#E746E1",
					},
				],
			},
		],
	})
})

landBtn.click()
