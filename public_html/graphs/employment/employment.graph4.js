$wt.charts
	.render({
		data: {
			chart: {
				type: "column",
				inverted: false,
				zoomType: "x",
				backgroundColor: "#F3F6FC",
				panning: {
					enabled: true,
					type: "y",
				},
				panKey: "shift",
				height: 700,
			},
			colors: ["#BCBC01", "#39B0B5", "#4C7DBF"],
			legend: {
				itemStyle: {
					fontFamily: "Arial",
				},
			},
      tooltip: {
        backgroundColor: "#3C3C3C",
				style: {
					fontFamily: "Arial",
					color: "#fff",
				},
      },
			plotOptions: {
				column: {
					pointWidth: 12,
					groupPadding: 10, // Reduce the padding between series
					pointPadding: 10, // Reduce the padding between columns
				},
				series: {
					stacking: "percent",
					dataLabels: {
						enabled: true,
					},
				},
				column: {
					cursor: "pointer",
					events: {
						click: function (event) {
							const extraInfo = document.getElementById("chart-extra-info")
							const content = extraInfo.querySelector("p")
							content.textContent = `${event.point.series.name} - ${event.point.accessibility.valueDescription} Extra info can be added here.`
							extraInfo.style.display = "block"
						},
					},
				},
			},
			series: [
				{
					name: "15-29 years (%)",
					data: [
						{
							name: "Malta",
							y: 26.4,
						},
						{
							name: "Finland",
							y: 20,
						},
						{
							name: "Netherlands",
							y: 19.5,
						},
						{
							name: "Iceland",
							y: 19.4,
						},
						{
							name: "Norway",
							y: 19.3,
						},
						{
							name: "Switzerland",
							y: 17.1,
						},
						{
							name: "Poland",
							y: 16.9,
						},
						{
							name: "Denmark",
							y: 16.4,
						},
						{
							name: "Luxembourg",
							y: 15.4,
						},
						{
							name: "Lithuania",
							y: 14.7,
						},
						{
							name: "France",
							y: 13.3,
						},
						{
							name: "Germany",
							y: 13,
						},
						{
							name: "Austria",
							y: 12.7,
						},
						{
							name: "Sweden",
							y: 12.7,
						},
						{
							name: "Romania",
							y: 12.3,
						},
						{
							name: "EU Average",
							y: 12.1,
						},
						{
							name: "Croatia",
							y: 11.4,
						},
						{
							name: "Czechia",
							y: 11,
						},
						{
							name: "Belgium",
							y: 10.9,
						},
						{
							name: "Hungary",
							y: 10.7,
						},
						{
							name: "Cyprus",
							y: 10.6,
						},
						{
							name: "Slovakia",
							y: 10.6,
						},
						{
							name: "Greece",
							y: 10.5,
						},
						{
							name: "Slovenia",
							y: 10.5,
						},
						{
							name: "Ireland",
							y: 10.4,
						},
						{
							name: "Estonia",
							y: 9.4,
						},
						{
							name: "Latvia",
							y: 9.4,
						},
						{
							name: "Italy",
							y: 9,
						},
						{
							name: "Portugal",
							y: 8.9,
						},
						{
							name: "Bulgaria",
							y: 8.2,
						},
						{
							name: "Spain",
							y: 8,
						},
					],
				},
				{
					name: "30-49 years (%)",
					data: [
						{
							name: "Malta",
							y: 59.5,
						},
						{
							name: "Finland",
							y: 40,
						},
						{
							name: "Netherlands",
							y: 40.5,
						},
						{
							name: "Iceland",
							y: 47.3,
						},
						{
							name: "Norway",
							y: 38.7,
						},
						{
							name: "Switzerland",
							y: 44.9,
						},
						{
							name: "Poland",
							y: 52.3,
						},
						{
							name: "Denmark",
							y: 38.6,
						},
						{
							name: "Luxembourg",
							y: 62.6,
						},
						{
							name: "Lithuania",
							y: 52.3,
						},
						{
							name: "France",
							y: 54.2,
						},
						{
							name: "Germany",
							y: 44,
						},
						{
							name: "Austria",
							y: 47.3,
						},
						{
							name: "Sweden",
							y: 46.3,
						},
						{
							name: "Romania",
							y: 56.9,
						},
						{
							name: "EU Average",
							y: 51,
						},
						{
							name: "Croatia",
							y: 53.6,
						},
						{
							name: "Czechia",
							y: 53,
						},
						{
							name: "Belgium",
							y: 55.1,
						},
						{
							name: "Hungary",
							y: 51.3,
						},
						{
							name: "Cyprus",
							y: 38.4,
						},
						{
							name: "Slovakia",
							y: 48.8,
						},
						{
							name: "Greece",
							y: 50.5,
						},
						{
							name: "Slovenia",
							y: 55.5,
						},
						{
							name: "Ireland",
							y: 48.6,
						},
						{
							name: "Estonia",
							y: 39.4,
						},
						{
							name: "Latvia",
							y: 48.6,
						},
						{
							name: "Italy",
							y: 52,
						},
						{
							name: "Portugal",
							y: 53.1,
						},
						{
							name: "Bulgaria",
							y: 53.8,
						},
						{
							name: "Spain",
							y: 51,
						},
					],
				},
				{
					name: "50-64 years (%)",
					data: [
						{
							name: "Malta",
							y: 14.1,
						},
						{
							name: "Finland",
							y: 40,
						},
						{
							name: "Netherlands",
							y: 40,
						},
						{
							name: "Iceland",
							y: 33.3,
						},
						{
							name: "Norway",
							y: 42,
						},
						{
							name: "Switzerland",
							y: 38,
						},
						{
							name: "Poland",
							y: 30.8,
						},
						{
							name: "Denmark",
							y: 45,
						},
						{
							name: "Luxembourg",
							y: 22,
						},
						{
							name: "Lithuania",
							y: 33,
						},
						{
							name: "France",
							y: 32.5,
						},
						{
							name: "Germany",
							y: 43,
						},
						{
							name: "Austria",
							y: 40,
						},
						{
							name: "Sweden",
							y: 41,
						},
						{
							name: "Romania",
							y: 30.8,
						},
						{
							name: "EU Average",
							y: 36.9,
						},
						{
							name: "Croatia",
							y: 35,
						},
						{
							name: "Czechia",
							y: 36,
						},
						{
							name: "Belgium",
							y: 34,
						},
						{
							name: "Hungary",
							y: 38,
						},
						{
							name: "Cyprus",
							y: 51,
						},
						{
							name: "Slovakia",
							y: 40.6,
						},
						{
							name: "Greece",
							y: 39,
						},
						{
							name: "Slovenia",
							y: 34,
						},
						{
							name: "Ireland",
							y: 41,
						},
						{
							name: "Estonia",
							y: 51.2,
						},
						{
							name: "Latvia",
							y: 42,
						},
						{
							name: "Italy",
							y: 39,
						},
						{
							name: "Portugal",
							y: 38,
						},
						{
							name: "Bulgaria",
							y: 38,
						},
						{
							name: "Spain",
							y: 41,
						},
					],
				},
				{
					name: "Malta",
					color: "#7DD0D3",
					marker: {
						symbol: "square",
					},
					data: [],
					showInLegend: true,
				},
			],
			xAxis: {
				type: "category",
				plotBands: [
					{
						color: "#7DD0D3",
						from: -0.5, // Start of the plot band
						to: 0.5, // End of the plot band
						id: "plot-band-2",
						label: {
							text: "Malta",
							style: {
								opacity: 0,
							},
						},
					},
				],
			},
		},
	})
	.ready(function (chart) {
		const originalData = chart.series.map(serie => serie.data.map(point => point.options));
    const originalPlotBands = chart.xAxis[0].userOptions.plotBands;
    const countryNames = chart.series[0].data.map(item => item.name);
    
    const selectElement = document.getElementById('employment-graph4-countries');
    countryNames.forEach(countryName => {
      const optionElement = document.createElement('option');
      optionElement.value = countryName;
      optionElement.text = countryName;
      
      selectElement.appendChild(optionElement);
    });

    // Listen for the change event
    selectElement.addEventListener('change', function() {
      const selectedCountry = this.value;
    
      // If the selected value is 'All countries', reset the filter
      if (selectedCountry === 'All countries') {
        chart.series.forEach((serie, i) => {
          serie.setData(originalData[i], false);
        });
    
        // Restore the original plotBands
        chart.xAxis[0].update({
          plotBands: originalPlotBands
        });
      } else {
        // Otherwise, filter the series data and update the chart
        chart.series.forEach((serie, i) => {
          const filteredData = originalData[i].filter(point => point.name === selectedCountry);
          serie.setData(filteredData, false);
        });
    
        // Disable the plotBands
        chart.xAxis[0].update({
          plotBands: []
        });
      }
    
      // Redraw the chart after all series have been updated
      chart.redraw();
    });
	})
