$wt.charts
	.render({
		data: {
			chart: {
				type: "column",
				backgroundColor: "#F3F6FC",
				inverted: false,
				zoomType: "x",
        panning: {
          enabled: true,
          type: "x"
        },
        panKey: "shift",
        height: 600
			},
			colors: ["#4C7DBF", "#39B0B5", "#e6a532"],
			legend: {
				align: "center",
			},
			plotOptions: {
				series: {
					stacking: "percent",
					dataLabels: {
						style: {
							fontWeight: "bold",
						},
					},
				},
        column: {
          cursor: 'pointer',
          events: {
            click: function(event) {
              const popup = document.getElementById('popup');
              const content = document.getElementById('popup-content');
              content.textContent = `${event.point.accessibility.valueDescription}`
              popup.style.left = event.point.plotX + 'px';
              popup.style.top = event.point.yBottom + 'px';
              popup.style.display = 'block';
            }
          }
        }
			},
      series: [
				{
					name: "Land Value (%)",
					data: [
						{
							name: "Slovenia",
							y: 98.9
						},
						{
							name: "Lithuania",
							y: 97.6,
						},
						{
							name: "Slovakia",
							y: 97.2,
						},
						{
							name: "Hungary",
							y: 96.7,
						},
						{
							name: "Poland",
							y: 96.6,
						},
						{
							name: "Romania",
							y: 95.6,
						},
						{
							name: "Czechia",
							y: 94.8,
						},
						{
							name: "Sweden",
							y: 92.1,
						},
						{
							name: "Austria",
							y: 91.6,
						},
						{
							name: "Switzerland",
							y: 90.1,
						},
						{
							name: "Italy",
							y: 89.7,
						},
						{
							name: "Belgium",
							y: 89.6,
						},
						{
							name: "Spain",
							y: 89.4,
						},
						{
							name: "France",
							y: 89.2,
						},
						{
							name: "Finland",
							y: 88,
						},
						{
							name: "Germany",
							y: 87,
						},
						{
							name: "Bulgaria",
							y: 87,
						},
						{
							name: "Portugal",
							y: 86.9,
						},
						{
							name: "Estonia",
							y: 86.2,
						},
						{
							name: "Cuprus",
							y: 83.2,
						},
						{
							name: "Ireland",
							y: 82.3,
						},
						{
							name: "Denmark",
							y: 79.9,
						},
						{
							name: "Latvia",
							y: 79.2,
						},
						{
							name: "Netherlands",
							y: 78.6,
						},
						{
							name: "Croatia",
							y: 76.9,
						},
						{
							name: "Luxembourg",
							y: 70.4,
						},
						{
							name: "Norway",
							y: 68.1,
						},
						{
							name: "Greece",
							y: 64.5,
						},
						{
							name: "Malta",
							y: 46.4,
						},
						{
							name: "Iceland",
							y: 35,
						},
					],
				},
				{
					name: "Water and Air Value (%)",
					data: [
						{
							name: "Slovenia",
							y: 1.1,
						},
						{
							name: "Lithuania",
							y: 2.4,
						},
						{
							name: "Slovakia",
							y: 2.8,
						},
						{
							name: "Hungary",
							y: 3.3,
						},
						{
							name: "Poland",
							y: 3.4,
						},
						{
							name: "Romania",
							y: 4.4,
						},
						{
							name: "Czechia",
							y: 5.2,
						},
						{
							name: "Sweden",
							y: 7.9,
						},
						{
							name: "Austria",
							y: 8.4,
						},
						{
							name: "Switzerland",
							y: 9.9,
						},
						{
							name: "Italy",
							y: 10.3,
						},
						{
							name: "Belgium",
							y: 10.4,
						},
						{
							name: "Spain",
							y: 10.6,
						},
						{
							name: "France",
							y: 10.8,
						},
						{
							name: "Finland",
							y: 12,
						},
						{
							name: "Germany",
							y: 13,
						},
						{
							name: "Bulgaria",
							y: 13,
						},
						{
							name: "Portugal",
							y: 13.1,
						},
						{
							name: "Estonia",
							y: 13.8,
						},
						{
							name: "Cuprus",
							y: 16.8,
						},
						{
							name: "Ireland",
							y: 17.7,
						},
						{
							name: "Denmark",
							y: 20.1,
						},
						{
							name: "Latvia",
							y: 20.8,
						},
						{
							name: "Netherlands",
							y: 21.4,
						},
						{
							name: "Croatia",
							y: 23.1,
						},
						{
							name: "Luxembourg",
							y: 29.6,
						},
						{
							name: "Norway",
							y: 31.9,
						},
						{
							name: "Greece",
							y: 35.5,
						},
						{
							name: "Malta",
							y: 53.6,
						},
						{
							name: "Iceland",
							y: 65
						},
					],
				},
        {
          name: 'Highest Water and Air value',
          color: '#7DD0D3',
          marker: {
            symbol: 'square'
          },
          data: [],
          showInLegend: true
        },
         {
          name: 'Highest Land value',
          color: 'rgba(76, 125, 191, 0.58)',
          marker: {
            symbol: 'square'
          },
          data: [],
          showInLegend: true
         }
			],
			title: {
				text: " Employment distribution by transport subsector in EU Member States 2021",
				style: {
					fontFamily: "Arial",
					fontSize: "15px",
				},
			},
      tooltip: {
        backgroundColor: "#3C3C3C",
				style: {
					fontFamily: "Arial",
					color: "#fff",
				},
      },  
      xAxis: {
				type: "category",
				gridLineWidth: 1,
				gridLineColor: "#CFDAF5",
				plotBands: [
					{
						color: '#7DD0D3',
						from: 28.5, // Start of the plot band
						to: 29.5, // End of the plot band
						id: "plot-band-1",
						label: {
							text: "Highest Water and Air value",
							style: {
                opacity: 0
							},
						},
					},
          {
						color: 'rgba(76, 125, 191, 0.58)',
						from: -0.5, // Start of the plot band
						to: 0.5, // End of the plot band
						id: "plot-band-2",
						label: {
							text: "Highest Land value",
							style: {
                opacity: 0
							},
						},
					},
				],
			},
			yAxis: {
				gridLineColor: "#CFDAF5",
			},
		}
	})
	.ready(function (chart) {
    // Handles closing the popup.
    document.getElementById('popup-close').addEventListener('click', function(event) {
      this.parentElement.parentElement.style.display = 'none';
    });

    const originalData = chart.series.map(serie => serie.data.map(point => point.options));
    const originalPlotBands = chart.xAxis[0].userOptions.plotBands;
    const countryNames = chart.series[0].data.map(item => item.name);
    
    const selectElement = document.getElementById('employment-graph2-countries');
    countryNames.forEach(countryName => {
      const optionElement = document.createElement('option');
      optionElement.value = countryName;
      optionElement.text = countryName;
      
      selectElement.appendChild(optionElement);
    });

    // Listen for the change event
    selectElement.addEventListener('change', function() {
      const selectedCountry = this.value;
    
      // If the selected value is 'All countries', reset the filter
      if (selectedCountry === 'All countries') {
        chart.series.forEach((serie, i) => {
          serie.setData(originalData[i], false);
        });
    
        // Restore the original plotBands
        chart.xAxis[0].update({
          plotBands: originalPlotBands
        });
      } else {
        // Otherwise, filter the series data and update the chart
        chart.series.forEach((serie, i) => {
          const filteredData = originalData[i].filter(point => point.name === selectedCountry);
          serie.setData(filteredData, false);
        });
    
        // Disable the plotBands
        chart.xAxis[0].update({
          plotBands: []
        });
      }
    
      // Redraw the chart after all series have been updated
      chart.redraw();
    });
	})
