const baseColor = "#0E47CB"
$wt.map
	.render({
		map: {
			background: ["graybg"],
			zoom: 4,
			maxZoom: 5,
			minZoom: 4,
      center: [48, 9]
		},
		layers: {
			chorojson: [
				{
					data: {
						source: {
							AT: {
								value: 96,
								text: "Austria",
							},
							BE: {
								value: 150,
								text: "Belgium",
							},
							BG: {
								value: 159,
								text: "Bulgaria",
							},
							HR: {
								value: 71,
								text: "Croatia",
							},
							CY: {
								value: 5,
								text: "Cyprus",
							},
							CZ: {
								value: 207,
								text: "Czechia",
							},
							DK: {
								value: 69,
								text: "Denmark",
							},
							EE: {
								value: 27,
								text: "Estonia",
							},
							EU: {
								value: 6222,
								text: "EU",
							},
							FI: {
								value: 78,
								text: "Finland",
							},
							FR: {
								value: 842,
								text: "France",
							},
							DE: {
								value: 689,
								text: "Germany",
							},
							EL: {
								value: 147,
								text: "Greece",
							},
							HU: {
								value: 191,
								text: "Hungary",
							},
							IS: {
								value: 6,
								text: "Iceland",
							},
							IE: {
								value: 54,
								text: "Ireland",
							},
							IT: {
								value: 626,
								text: "Italy",
							},
							LV: {
								value: 41,
								text: "Latvia",
							},
							LT: {
								value: 67,
								text: "Lithuania",
							},
							LU: {
								value: 10,
								text: "Luxembourg",
							},
							MT: {
								value: 6,
								text: "Malta",
							},
							NL: {
								value: 253,
								text: "Netherlands",
							},
							NO: {
								value: 78,
								text: "Norway",
							},
							PL: {
								value: 792,
								text: "Poland",
							},
							PT: {
								value: 136,
								text: "Portugal",
							},
							RO: {
								value: 450,
								text: "Romania",
							},
							SK: {
								value: 99,
								text: "Slovakia",
							},
							SI: {
								value: 25,
								text: "Slovenia",
							},
							ES: {
								value: 627,
								text: "Spain",
							},
							SE: {
								value: 120,
								text: "Sweden",
							},
							CH: {
								value: 101,
								text: "Switzerland",
							},
						},
					},
					options: {
						ranges: [
							{
								from: 5,
								to: 150,
								style: "theme_1_20",
								legend: "from 5 to 150",
							},
							{
								from: 150,
								to: 290,
								style: "theme_1_40",
								legend: "from 150 to 290",
							},
							{
								from: 290,
								to: 430,
								style: "theme_1_60",
								legend: "from 290 to 430",
							},
							{
								from: 430,
								to: 570,
								style: "theme_1_80",
								legend: "from 430 to 570",
							},
							{
								from: 570,
								to: 710,
								style: "theme_1_100",
								legend: "from 570 to 710",
							},
							{
								from: 710,
								to: 850,
								style: "theme_1_+",
								legend: "from 710 to 850",
							},
							{
								legend: "Highest in EU (France)",
								style: {
									fillColor: "#39B0B5",
									fillOpacity: 1,
									color: "#39B0B5",
								},
							},
							{
								legend: "Lowest in EU (Cyprus)",
								style: {
									fillColor: "#BCBC01",
									fillOpacity: 1,
									color: "#BCBC01",
								},
							},
						],
						legend: {
							position: "bottomleft",
						},
						events: {
							click: {
								type: "info",
								content: "<h2>{text}</h2> {value}",
							},
						},
					},
				},
			],
		},
	})
	.ready(function (map) {
		map.on("layeradd", function (event) {
			const layer = event.layer
			if (layer.feature && layer.feature.properties && layer.feature.properties.value) {
				layer.unbindTooltip()
				const countryCode = layer.feature.properties.countryId
        // Hardcoded values will be replaced with automatically calculated min and max values on production.
				if (countryCode === "FR") {
					layer.setStyle({
						fillColor: "#39B0B5",
					})
				} else if (countryCode === "CY") {
					layer.setStyle({
						fillColor: "#BCBC01",
					})
				}

				let layerInitialStyle = layer._path.getAttribute("fill")
				const countryValue = parseInt(layer.feature.properties.value)
				const size = countryValue < 25 ? 15 : countryValue < 50 ? 20 : countryValue < 60 ? 25 : countryValue < 200 ? 30 : countryValue < 250 ? 40 : countryValue < 800 ? 50 : 60
        // Creates a circle marker for each country, with a different color for France and Cyprus.
				const countryCircle = L.circleMarker(layer.feature.properties.CENTROID, {
					fillColor: countryCode === "FR" ? "#187175" : countryCode === "CY" ? "#880" : baseColor,
					color: 'rgba(255,255,255,0.5)',
					fillOpacity: 1,
					radius: size / 2, 
				})
					.bindTooltip(`${layer.feature.properties.text}`, { direction: "top", offset: L.point(0, -10) })
					.openTooltip()
					.addTo(map)

				// Add the text as a separate marker
				L.marker(layer.feature.properties.CENTROID, {
					icon: L.divIcon({
						className: `country-labels country-${layer.feature.properties.countryId}`,
						html: layer.feature.properties.value,
						iconSize: [size, size],
					}),
				}).addTo(map)

        // Register mouseover and mouseout events for the country layer and the circle marker, to properly handle UI updates.
				layer.on("mouseover", () => mouseoverCallback(layer, countryCircle))
				countryCircle.on("mouseover", () => mouseoverCallback(layer, countryCircle))
				layer.on("mouseout", () => mouseoutCallback(layer, countryCircle, layerInitialStyle))
				countryCircle.on("mouseout", () => mouseoutCallback(layer, countryCircle, layerInitialStyle))
			}
		})

		L.circleMarker([50.639939055395864, 29.091796875], {
			fillColor: baseColor,
			color: 'rgba(255,255,255,0.5)',
			fillOpacity: 1,
			radius: 40, 
		})
    .bindTooltip(`The global percentage across EU.`, { direction: "top", offset: L.point(0, -10) })
    .openTooltip()
    .addTo(map)

		// Add the text as a separate marker
		L.marker([50.639939055395864, 29.091796875], {
			icon: L.divIcon({
				className: `country-labels country-eu`,
				html: "6222 (EU)",
				iconSize: [80, 80],
			}),
		}).addTo(map)
	})

/**
 * Sets the styles for the country layer and the circle marker when the mouse is over the country layer.
 * @param {*} layer 
 * @param {*} countryCircle 
 */
const mouseoverCallback = function (layer, countryCircle) {
	countryCircle.setStyle({
		color: "#fff",
		fillColor: "#fff",
		fillOpacity: 1,
	})
	document.querySelector(`.country-${layer.feature.properties.countryId}`).style.color = baseColor
	layer.setStyle({
		fillColor: baseColor,
	})
}
/**
 * Sets the styles for the country layer and the circle marker when the mouse is out of the country layer.
 * @param {*} layer 
 * @param {*} countryCircle 
 * @param {*} layerInitialStyle 
 */
const mouseoutCallback = function (layer, countryCircle, layerInitialStyle) {
	countryCircle.setStyle({
		transition: "fillColor 0.5s",
		color: 'rgba(255,255,255,0.5)',
		fillColor: baseColor,
		fillOpacity: 1,
	})
	document.querySelector(`.country-${layer.feature.properties.countryId}`).style.color = "#fff"
	layer.setStyle({
		fillColor: layerInitialStyle,
	})
}
