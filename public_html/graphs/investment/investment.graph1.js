$wt.charts
	.render({
		data: {
			chart: {
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy"
        },
        panKey: "shift",
			},
			colors: ["#4C7DBF", "#BCBC01", "#39B0B5", "#e6aa82", "#829bcd", "#fae6c3", "#aab9e1"],
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
						style: {
							fontWeight: "bold",
						},
					},
				},
			},
			series: [
				{
					name: "European Union - 27 countries (from 2020)",
					data: [
						{
							name: "2020",
							y: 1.7,
						},
						{
							name: "2019",
							y: 2,
						},
						{
							name: "2018",
							y: 2,
						},
						{
							name: "2017",
							y: 2,
						},
						{
							name: "2016",
							y: 1.9,
						},
						{
							name: "2015",
							y: 1.7,
						},
						{
							name: "2014",
							y: 1.6,
						},
						{
							name: "2013",
							y: 1.5,
						},
						{
							name: "2012",
							y: 1.6,
						},
						{
							name: "2011",
							y: 1.7,
						},
						{
							name: "2021",
							y: 1.6,
						},
					],
				},
			],
			title: {
				text: "Investment in transport equipment ",
				style: {
					fontSize: "14px",
				},
			},
			subtitle: {
				text: "(%, of GDP, EU, 2011–2021)",
			},
			tooltip: {
				pointFormat: '<span style="color:{point.color}">●</span> {series.name}: <b>{point.y}%</b><br/>',
			},
			xAxis: {
				type: "category",
				gridLineColor: "#CFDAF5",
			},
			yAxis: {
				gridLineColor: "#CFDAF5",
        title: {
          text: "Percentage of GDP"
        }
			},
		},
	})
	.ready(function (chart) {
	})
