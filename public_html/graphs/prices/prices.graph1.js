$wt.charts
	.render({
		data: {
			chart: {
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy",
        },
        panKey: "shift",
			},
			colors: ["#BCBC01", "#39B0B5", "#4C7DBF", "#e6aa82", "#829bcd", "#fae6c3", "#aab9e1"],
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
						style: {
							fontWeight: "bold",
						},
					},
				},
			},
      yAxis: {
        gridLineColor: "#CFDAF5",
        plotLines: [{
          color: '#000', 
          width: 2, 
          value: 100, 
          label: {
            text: 'EU Average', 
            align: 'right', 
            y: 15, 
            x: 0,
            style: {
              color: '#000', 
              fontWeight: 'bold'
            }
          }
        }]
      },
			series: [
				{
					name: "2021",
					data: [
						{
							name: "Belgium",
							y: 105.3,
						},
						{
							name: "Bulgaria",
							y: 70.3,
						},
						{
							name: "Czechia",
							y: 79.2,
						},
						{
							name: "Denmark",
							y: 124.6,
						},
						{
							name: "Germany",
							y: 106.5,
						},
						{
							name: "Estonia",
							y: 86.9,
						},
						{
							name: "Ireland",
							y: 109.4,
						},
						{
							name: "Greece",
							y: 88.5,
						},
						{
							name: "Spain",
							y: 92.2,
						},
						{
							name: "France",
							y: 107.4,
						},
						{
							name: "Croatia",
							y: 81.4,
						},
						{
							name: "Italy",
							y: 99.5,
						},
						{
							name: "Cyprus",
							y: 85.4,
						},
						{
							name: "Latvia",
							y: 79.1,
						},
						{
							name: "Lithuania",
							y: 79.5,
						},
						{
							name: "Luxembourg",
							y: 97.8,
						},
						{
							name: "Hungary",
							y: 77.7,
						},
						{
							name: "Malta",
							y: 86.1,
						},
						{
							name: "Netherlands",
							y: 115.8,
						},
						{
							name: "Austria",
							y: 107.2,
						},
						{
							name: "Poland",
							y: 72.1,
						},
						{
							name: "Portugal",
							y: 93.7,
						},
						{
							name: "Romania",
							y: 72.5,
						},
						{
							name: "Slovenia",
							y: 85.4,
						},
						{
							name: "Slovakia",
							y: 83.4,
						},
						{
							name: "Finland",
							y: 115.4,
						},
						{
							name: "Sweden",
							y: 130.2,
						},
						{
							name: "Iceland",
							y: 127.6,
						},
						{
							name: "Norway",
							y: 130.2,
						},
						{
							name: "Switzerland",
							y: 119.1,
						},
					],
				},
				{
					name: "2019",
					data: [
						{
							name: "Belgium",
							y: 104.1,
						},
						{
							name: "Bulgaria",
							y: 68.5,
						},
						{
							name: "Czechia",
							y: 77,
						},
						{
							name: "Denmark",
							y: 128.6,
						},
						{
							name: "Germany",
							y: 107.5,
						},
						{
							name: "Estonia",
							y: 82.2,
						},
						{
							name: "Ireland",
							y: 107.5,
						},
						{
							name: "Greece",
							y: 89.2,
						},
						{
							name: "Spain",
							y: 92.5,
						},
						{
							name: "France",
							y: 107.5,
						},
						{
							name: "Croatia",
							y: 84.5,
						},
						{
							name: "Italy",
							y: 99.3,
						},
						{
							name: "Cyprus",
							y: 86.6,
						},
						{
							name: "Latvia",
							y: 77.4,
						},
						{
							name: "Lithuania",
							y: 77.7,
						},
						{
							name: "Luxembourg",
							y: 92.4,
						},
						{
							name: "Hungary",
							y: 77.9,
						},
						{
							name: "Malta",
							y: 85.6,
						},
						{
							name: "Netherlands",
							y: 120,
						},
						{
							name: "Austria",
							y: 108.1,
						},
						{
							name: "Poland",
							y: 71.6,
						},
						{
							name: "Portugal",
							y: 95.6,
						},
						{
							name: "Romania",
							y: 71.8,
						},
						{
							name: "Slovenia",
							y: 88.5,
						},
						{
							name: "Slovakia",
							y: 79.3,
						},
						{
							name: "Finland",
							y: 112.8,
						},
						{
							name: "Sweden",
							y: 113,
						},
						{
							name: "Iceland",
							y: 131.3,
						},
						{
							name: "Norway",
							y: 142.1,
						},
						{
							name: "Switzerland",
							y: 121,
						},
					],
				},
        {
          name: "Lowest price level",
        },
        {
          name: "Highest price level",
        }
			],
			title: {
				text: "Price level index for transport",
				style: {
					fontSize: "16px",
				},
			},
			subtitle: {
				text: "(Comparison for years 2019 + 2021)",
			},
			tooltip: {
				pointFormat: '<span style="color:{point.color}">●</span> {series.name}: <b>{point.y}%</b><br/>',
        backgroundColor: "#3C3C3C",
				style: {
					fontFamily: "Arial",
					color: "#fff",
				},
			},
			xAxis: {
				type: "category",
        gridLineColor: "#CFDAF5",
        plotBands: [
					{
						color: "#4C7DBF", 
						from: 0.5, // Start of the plot band
						to: 1.5, // End of the plot band
						id: "plot-band-1",
						label: {
							text: "Lowest price",
							style: {
								opacity: 0,
							},
						},
					},
          {
            color: "#e6aa82", 
            from: 27.5,
            to: 28.5,
            id: "plot-band-2",
            label: {
              text: "Highest price",
              style: {
                opacity: 0
              }
            }
          }
				],
			},
		},
	})
	.ready(function (chart) {
    const originalData = chart.series.map(serie => serie.data.map(point => point.options));
    const originalPlotBands = chart.yAxis[0].userOptions.plotBands;
    const countryNames = chart.series[0].data.map(item => item.name);
    
    const selectElement = document.getElementById('prices-graph1-countries');
    countryNames.forEach(countryName => {
      const optionElement = document.createElement('option');
      optionElement.value = countryName;
      optionElement.text = countryName;
      
      selectElement.appendChild(optionElement);
    });

    // Listen for the change event
    selectElement.addEventListener('change', function() {
      const selectedCountry = this.value;
    
      // If the selected value is 'All countries', reset the filter
      if (selectedCountry === 'All countries') {
        chart.series.forEach((serie, i) => {
          serie.setData(originalData[i], false);
        });
    
        // Restore the original plotBands
        chart.yAxis[0].update({
          plotBands: originalPlotBands
        });
      } else {
        // Otherwise, filter the series data and update the chart
        chart.series.forEach((serie, i) => {
          const filteredData = originalData[i].filter(point => point.name === selectedCountry);
          serie.setData(filteredData, false);
        });
    
        // Disable the plotBands
        chart.yAxis[0].update({
          plotBands: []
        });
      }
    
      // Redraw the chart after all series have been updated
      chart.redraw();
    });
	})
