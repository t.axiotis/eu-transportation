$wt.charts
	.render({
		data: {
			chart: {
				type: "column",
				backgroundColor: "#F3F6FC",
				zoomType: "xy",
        panning: {
          enabled: true,
          type: "xy",
        },
        panKey: "shift",
			},
			colors: ["#39B0B5", "#4C7DBF", "#e6a532", "#829bcd", "#e6aa82", "#fae6c3", "#aab9e1"],
			legend: {
				itemStyle: {
					fontFamily: "Arial",
				},
			},
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
						style: {
							fontFamily: "Arial",
							fontWeight: "bold",
						},
					},
				},
        column: {
          cursor: 'pointer',
          events: {
            click: function(event) {
              const popup = document.getElementById('popup');
              const content = document.getElementById('popup-content');
              console.log(event.point)
              content.textContent = `${event.point.accessibility.valueDescription}`
              popup.style.left = event.point.plotX + 'px';
              popup.style.top = popup.clientHeight + event.point.plotY + 'px';
              popup.style.display = 'block';
            }
          }
        },
			},
			series: [
				{
					name: "2020",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: -9.7,
						},
						{
							name: "Belgium",
							y: -8.4,
						},
						{
							name: "Bulgaria",
							y: -12.9,
						},
						{
							name: "Czechia",
							y: -11.4,
						},
						{
							name: "Denmark",
							y: -9.6,
						},
						{
							name: "Germany",
							y: -9.7,
						},
						{
							name: "Estonia",
							y: -10.5,
						},
						{
							name: "Ireland",
							y: -3.8,
						},
						{
							name: "Greece",
							y: -9,
						},
						{
							name: "Spain",
							y: -10,
						},
						{
							name: "France",
							y: -11,
						},
						{
							name: "Croatia",
							y: -9.3,
						},
						{
							name: "Italy",
							y: -8.2,
						},
						{
							name: "Cyprus",
							y: -8.8,
						},
						{
							name: "Latvia",
							y: -10.7,
						},
						{
							name: "Lithuania",
							y: -9.1,
						},
						{
							name: "Luxembourg",
							y: -11.1,
						},
						{
							name: "Hungary",
							y: -5.9,
						},
						{
							name: "Malta",
							y: -0.7,
						},
						{
							name: "Netherlands",
							y: -5.8,
						},
						{
							name: "Austria",
							y: -12.5,
						},
						{
							name: "Poland",
							y: -10.2,
						},
						{
							name: "Portugal",
							y: -7.5,
						},
						{
							name: "Romania",
							y: -14.8,
						},
						{
							name: "Slovenia",
							y: -15.4,
						},
						{
							name: "Slovakia",
							y: -11.5,
						},
						{
							name: "Finland",
							y: -7.5,
						},
						{
							name: "Sweden",
							y: -10,
						},
						{
							name: "Iceland",
							y: -6,
						},
						{
							name: "Norway",
							y: -6,
						},
						{
							name: "Switzerland",
							y: -10.6,
						},
					],
				},
				{
					name: "2021",
					data: [
						{
							name: "European Union - 27 countries (from 2020)",
							y: 17.2,
						},
						{
							name: "Belgium",
							y: 15.2,
						},
						{
							name: "Bulgaria",
							y: 17.2,
						},
						{
							name: "Czechia",
							y: 14.6,
						},
						{
							name: "Denmark",
							y: 16.5,
						},
						{
							name: "Germany",
							y: 22.4,
						},
						{
							name: "Estonia",
							y: 13.6,
						},
						{
							name: "Ireland",
							y: 11,
						},
						{
							name: "Greece",
							y: 13.1,
						},
						{
							name: "Spain",
							y: 16,
						},
						{
							name: "France",
							y: 12.6,
						},
						{
							name: "Croatia",
							y: 16.8,
						},
						{
							name: "Italy",
							y: 12.2,
						},
						{
							name: "Cyprus",
							y: 14.6,
						},
						{
							name: "Latvia",
							y: 17.4,
						},
						{
							name: "Lithuania",
							y: 18.2,
						},
						{
							name: "Luxembourg",
							y: 24.3,
						},
						{
							name: "Hungary",
							y: 22.3,
						},
						{
							name: "Malta",
							y: -2.4,
						},
						{
							name: "Netherlands",
							y: 16.8,
						},
						{
							name: "Austria",
							y: 17.1,
						},
						{
							name: "Poland",
							y: 22,
						},
						{
							name: "Portugal",
							y: 14.5,
						},
						{
							name: "Romania",
							y: 19.1,
						},
						{
							name: "Slovenia",
							y: 17.2,
						},
						{
							name: "Slovakia",
							y: 17.3,
						},
						{
							name: "Finland",
							y: 18.8,
						},
						{
							name: "Sweden",
							y: 16.7,
						},
						{
							name: "Iceland",
							y: 11.9,
						},
						{
							name: "Norway",
							y: 11.8,
						},
						{
							name: "Switzerland",
							y: 16.2,
						},
					],
				},
        {
          name: "Lowest changes",
        },
        {
          name: "Highest changes",
        }
			],
			title: {
				text: " Annual price change for transport fuels and lubricants ",
				style: {
					fontFamily: "Arial",
					fontSize: "16px",
				},
			},
			subtitle: {
				text: "(%, 2020 and 2021)",
				style: {
					fontFamily: "Arial",
				},
			},
			tooltip: {
				backgroundColor: "#3C3C3C",
				style: {
					fontFamily: "Arial",
					color: "#fff",
				},
			},
			xAxis: {
				type: "category",
				title: {
					style: {
						fontFamily: "Arial",
						color: "#2d50a0",
					},
				},
				gridLineColor: "#CFDAF5",
        plotBands: [
					{
						color: "#e6a532",
						from: 17.5, // Start of the plot band
						to: 18.5, // End of the plot band
						id: "plot-band-1",
						label: {
							text: "Lowest change",
							style: {
								opacity: 0,
							},
						},
					},
          {
            color: "#829bcd",
            from: 15.5,
            to: 16.5,
            id: "plot-band-2",
            label: {
              text: "Highest price",
              style: {
                opacity: 0
              }
            }
          }
				],
			},
			yAxis: {
				title: {
					style: {
						fontFamily: "Arial",
					},
				},
				gridLineColor: "#CFDAF5",
			},
		},
	})
	.ready(function (chart) {

     // Handles closing the popup.
     document.getElementById('popup-close').addEventListener('click', function(event) {
      this.parentElement.parentElement.style.display = 'none';
    });

    
    const originalData = chart.series.map(serie => serie.data.map(point => point.options));
    const originalPlotBands = chart.yAxis[0].userOptions.plotBands;
    const countryNames = chart.series[0].data.map(item => item.name);
    
    const selectElement = document.getElementById('prices-graph2-countries');
    countryNames.forEach(countryName => {
      const optionElement = document.createElement('option');
      optionElement.value = countryName;
      optionElement.text = countryName;
      
      selectElement.appendChild(optionElement);
    });

    // Listen for the change event
    selectElement.addEventListener('change', function() {
      const selectedCountry = this.value;
    
      // If the selected value is 'All countries', reset the filter
      if (selectedCountry === 'All countries') {
        chart.series.forEach((serie, i) => {
          serie.setData(originalData[i], false);
        });
    
        // Restore the original plotBands
        chart.yAxis[0].update({
          plotBands: originalPlotBands
        });
      } else {
        // Otherwise, filter the series data and update the chart
        chart.series.forEach((serie, i) => {
          const filteredData = originalData[i].filter(point => point.name === selectedCountry);
          serie.setData(filteredData, false);
        });
    
        // Disable the plotBands
        chart.yAxis[0].update({
          plotBands: []
        });
      }
    
      // Redraw the chart after all series have been updated
      chart.redraw();
    });
	})
