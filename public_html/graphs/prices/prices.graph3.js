$wt.charts
	.render({
		data: {
			chart: {
				backgroundColor: "#F3F6FC",
				zoomType: "x",
        panning: {
          enabled: true,
          type: "xy",
        },
        panKey: "shift",
			},
			colors: ["#466eb4", "#d7642d", "#e6a532", "#829bcd", "#e6aa82", "#fae6c3", "#aab9e1"],
			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
					},
				},
			},
			series: [
				{
					name: "Value",
					data: [
						{
							name: "2021",
							y: 104.7,
						},
						{
							name: "2020",
							y: 85,
						},
						{
							name: "2019",
							y: 98.1,
						},
						{
							name: "2018",
							y: 98.3,
						},
						{
							name: "2017",
							y: 92,
						},
						{
							name: "2016",
							y: 84,
						},
						{
							name: "2015",
							y: 90,
						},
						{
							name: "2014",
							y: 103.1,
						},
						{
							name: "2013",
							y: 105.2,
						},
						{
							name: "2012",
							y: 108.3,
						},
						{
							name: "2011",
							y: 100,
						},
					],
				},
			],
			xAxis: {
				type: "category",
				gridLineColor: "#CFDAF5",
			},
			yAxis: {
				title: {
					text: "% of GDP",
				},
				gridLineColor: "#CFDAF5",
			},
		},
	})
	.ready(function (chart) {
	})
